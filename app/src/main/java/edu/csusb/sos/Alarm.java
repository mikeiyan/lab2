package edu.csusb.sos;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Alarm extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.alarm);
        ((ImageButton) findViewById(R.id.btn_alarm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mp.isPlaying()) {

                } else {
                    mp.setVolume(10,10);
                    mp.start();
                    nextpage();
                }
            }
        });

        ((Button) findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.pause();
                nextpage();
            }
        });
    }

    private void nextpage() {
        startActivity(new Intent(this, SelectSerivce.class));
    }
}
