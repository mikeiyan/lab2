package edu.csusb.sos;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Jay Ryu on 11/18/16.
 */

public class ServiceAdapter extends BaseExpandableListAdapter{

    private Context mContext;
    private ArrayList<ServiceResource> serviceResources;
    private LayoutInflater inflater;

    public ServiceAdapter(Context mContext, ArrayList<ServiceResource> serviceResources) {
        this.mContext = mContext;
        this.serviceResources = serviceResources;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getGroupCount() {
        return serviceResources.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return serviceResources.get(groupPosition).servicelist.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return serviceResources.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return serviceResources.get(groupPosition).servicelist.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        ServiceResource s1 = (ServiceResource) getGroup(groupPosition);
        String name = s1.serviceName;

        if(convertView == null) {
            convertView  = inflater.inflate(R.layout.parent_servicelist,null);

        }


        TextView nameTv = (TextView) convertView.findViewById(R.id.parent_tv);
        nameTv.setText(name);
        RelativeLayout rl = (RelativeLayout)convertView.findViewById(R.id.parentlayout);
        if (groupPosition == serviceResources.size())
            rl.setBackgroundColor(Color.parseColor("#FFCCCC"));

        ImageView imgindicator = (ImageView) convertView.findViewById(R.id.imgindicator);

        if (isExpanded){
            imgindicator.setImageResource(R.drawable.uparrow);
        } else {
            imgindicator.setImageResource(R.drawable.downarrow);
        }


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.child_servicelist, null);
        }
        //get child/service name(which will be call)
        String child = (String)getChild(groupPosition, childPosition);

        //set child name
        TextView tvName = (TextView) convertView.findViewById(R.id.sName);
        ImageView imgPhone = (ImageView) convertView.findViewById(R.id.phoneimg);

        tvName.setText(child);

        //get service name
        String serviceName = getGroup(groupPosition).toString();


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
