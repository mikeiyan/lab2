package edu.csusb.sos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by JayRyu on 11/13/16.
 */

public class SecurityTipsFragment extends Fragment {

    public SecurityTipsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_securitytips, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.tv_securityTips);
        textView.setText(Html.fromHtml(getString(R.string.securitytipstext)));
        return rootView;
    }
}
