package edu.csusb.sos;

import java.util.ArrayList;

/**
 * Created by JayRyu on 11/18/16.
 */

public class ServiceResource {

    //properties

    public String serviceName;
    public int id;
    public ArrayList<String> servicelist = new ArrayList<String>();

    public ServiceResource(String serviceName, int id) {
        this.serviceName = serviceName;
        this.id = id;
    }

    @Override
    public String toString() {
        return serviceName;
    }

    public int toint() {return id;}
}
