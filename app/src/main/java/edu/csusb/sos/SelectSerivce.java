package edu.csusb.sos;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.test.mock.MockPackageManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.Toast;

public class SelectSerivce extends AppCompatActivity{

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;
    protected double lat, lon;
    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = android.Manifest.permission.ACCESS_FINE_LOCATION;

    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_serivce);

        /**
         * Location Service
         */

        try {
            if (ActivityCompat.checkSelfPermission(this, mPermission) != MockPackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{mPermission}, REQUEST_CODE_PERMISSION);

                // If any permission above not allowed by user, this condition will execute every time, else your else part will work
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        gps = new GPSTracker(SelectSerivce.this);

        // check if GPS enabled
        if(gps.canGetLocation()){

            lat = gps.getLatitude();
            lon = gps.getLongitude();

            // \n is for new line
            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: "
                    + lat + "\nLong: " + lon, Toast.LENGTH_LONG).show();
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

        TabHost host = (TabHost)findViewById(R.id.tabHost);
        host.setup();

        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("Emergency");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Emergency");
        host.addTab(spec);

        //Tab 2
        spec = host.newTabSpec("Nonemergency");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Nonemergency");
        host.addTab(spec);


        /**
         *  Buttons
         */
        ((ImageButton) findViewById(R.id.shooting)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Shooting at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.knife)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Knife threatening at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.intruder)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Intruder at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.earthquake)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Earthquake at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.bomb)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Bomb at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.stalker)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Stalker at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.biohazard)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Biohazard at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.disease)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Disease at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.tornado)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Tornado at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.hurricane)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Hurricane at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.sexual)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Sexual harassment help please";
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.theft)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Theft at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.muggling)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Muggling at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.gunshot)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Gunshot at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.weapon)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Suspicious weapon at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.shooting)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Shooting at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.illegal)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Illegal at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });

        ((ImageButton) findViewById(R.id.abusive)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Abusive at latitude" + lat + "longitude" + lon;
                sendSMS(msg);
            }
        });


    }



    private void sendSMS(String message){

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage("9096426017",null,message,null,null);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        startActivity(new Intent(this, Alarm.class));

    }

}
