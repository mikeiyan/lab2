package edu.csusb.sos;

import android.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by JayRyu on 11/13/16.
 */

public class ResourcesFragment extends Fragment {

    HashMap<String, List<String>> Resources;
    List<String> optionlist;
    ExpandableListView Exp_list;
    ResourcesAdapter adapter;

    String[] permissionCall = {android.Manifest.permission.CALL_PHONE};

    String website;
    String pNumber;

    public ResourcesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_resources, container, false);

        Exp_list = (ExpandableListView) rootView.findViewById(R.id.Exp_listView);
        Resources = DataProvider.getInfo();
        optionlist = new ArrayList<>(Resources.keySet());

        adapter = new ResourcesAdapter(getActivity().getApplicationContext(), Resources, optionlist);

        Exp_list.setAdapter(adapter);
        Exp_list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                getInfo(groupPosition);
                clickaction(childPosition);
                return false;
            }
        });
        return rootView;
    }

    private void clickaction(int actionNum) {
        switch (actionNum) {
            case 0:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());

                builder.setTitle("Warning");
                builder.setMessage("By clicking this, you will visit website");
                builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent openWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
                        startActivity(openWeb);
                    }
                });
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.create();
                builder.show();

                break;
            case 1:
                String pnum = "tel:" + pNumber;
                callwarningbox(pnum);
                break;
        }

    }

    private void callwarningbox(final String pnum) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());

        builder.setTitle("Warning");
        builder.setMessage("By clicking continue, you can make outgoing call");
        builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(pnum));
                if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(getActivity(), permissionCall, 0);
                    return;

                }
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create();
        builder.show();
    }

    public void getInfo(int position) {
        switch (position) {
            case 0:
                website = "http://healthcenter.csusb.edu/";
                pNumber = "9095375241";
                break;
            case 1:
                website = "http://psychcounseling.csusb.edu/";
                pNumber = "9095375040";
                break;
            case 2:
                website = "http://studentunion.csusb.edu/departments/wrc";
                pNumber = "9095377203";
                break;
            case 3:
                website = "http://www.rainn.org/";
                pNumber = "8006564673";
                break;
            case 4:
                website = "http://www.ovw.usdoj.gov/";
                pNumber = null;
                break;
            case 5:
                website = "http://www.ncjrs.gov/index.html";
                pNumber = null;
                break;
            case 6:
                website = "http://ombuds.csusb.edu/";
                pNumber = "9095375635";
                break;
            case 7:
                website = "http://titleix.csusb.edu/";
                pNumber = "9095375669";
                break;


        }
    }

}

