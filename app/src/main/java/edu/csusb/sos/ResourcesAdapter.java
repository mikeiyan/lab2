package edu.csusb.sos;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by JayRyu on 12/4/16.
 */

public class ResourcesAdapter extends BaseExpandableListAdapter {

    private Context mcontext;
    private HashMap<String, List<String>> Resources;
    private List<String> options;
    ResourcesAdapter adapter;

    public ResourcesAdapter(Context mcontext, HashMap<String, List<String>> Resources, List<String> options) {

        this.mcontext = mcontext;
        this.Resources = Resources;
        this.options = options;

    }

    @Override
    public int getGroupCount() {
        return options.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return Resources.get(options.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return options.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return Resources.get(options.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String group_title = (String)getGroup(groupPosition);

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.parent_servicelist,parent,false);
        }

        TextView parent_text = (TextView) convertView.findViewById(R.id.parent_tv);
        parent_text.setTypeface(null, Typeface.BOLD);
        parent_text.setText(group_title);

        ImageView indicator = (ImageView) convertView.findViewById(R.id.imgindicator);

        if(isExpanded) {
            indicator.setImageResource(R.drawable.uparrow);
        } else {
            indicator.setImageResource(R.drawable.downarrow);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        String child_title = (String) getChild(groupPosition,childPosition);
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.child_servicelist,parent,false);
        }
        TextView child_textview = (TextView) convertView.findViewById(R.id.sName);
        child_textview.setText(child_title);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
