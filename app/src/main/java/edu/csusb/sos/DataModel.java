package edu.csusb.sos;

/**
 * Created by JayRyu on 11/13/16.
 */

public class DataModel {

    public int icon;
    public String name;

    public DataModel(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}
