package edu.csusb.sos;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import java.util.ArrayList;

/**
 * Created by Jay Ryu on 11/13/16.
 */

public class ServiceFragment extends Fragment {


    String[] permissionCall = {Manifest.permission.CALL_PHONE};

    public ServiceFragment(){}

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState) {
        View rootView = inflater.inflate(R.layout.fragment_services, container, false);

        ExpandableListView elv = (ExpandableListView) rootView.findViewById(R.id.expandableListView);

        ArrayList<ServiceResource> serviceResources = getData1();

        //create and bind to adapter
        ServiceAdapter adapter = new ServiceAdapter(getActivity().getApplicationContext(), serviceResources);
        elv.setAdapter(adapter);


        //set Onclick Listener
        elv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                switch(groupPosition) {
                    case 0:
                    case 1:
                        callwarningbox("tel:9095377777");
                        break;
                    case 2:
                        callwarningbox("tel:9095375165");
                        break;
                    case 3:
                        callwarningbox("tel:911");
                        break;
                }
                //callwarningbox("tel:9096426017");

                return false;
            }
        });



        return rootView;
    }

    private void callwarningbox(final String pnum) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Warning");
        builder.setMessage("By clicking continue, you can make outgoing call");
        builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(pnum));
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(getActivity(), permissionCall, 0);
                    return;

                }
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        Button pButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pButton.setTextColor(Color.parseColor("#000000"));
        Button nButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nButton.setTextColor(Color.parseColor("#000000"));

    }

    //add and get data
    private ArrayList<ServiceResource> getData1(){

        ServiceResource s1 = new ServiceResource("Car Unlock",1);
        s1.servicelist.add("Phone #");

        ServiceResource s2 = new ServiceResource("Battery Jump",2);
        s2.servicelist.add("Phone #");

        ServiceResource s3 = new ServiceResource("Escort",3);
        s3.servicelist.add("Phone #");

        ServiceResource s4 = new ServiceResource("Emergency",4);
        s4.servicelist.add("Phone #");

        ArrayList<ServiceResource> resourceArrayList = new ArrayList<>();
        resourceArrayList.add(s1);
        resourceArrayList.add(s2);
        resourceArrayList.add(s3);
        resourceArrayList.add(s4);
        return resourceArrayList;
    }


}
