package edu.csusb.sos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by JayRyu on 11/13/16.
 */

public class AnonymousTipsFragment extends Fragment {

    /**
     * Sending Email function was referenced :  http://sunil-android.blogspot.com/2013/07/send-email-via-gmail-with-java-mail-api.html
     **/
    private static String username = "csusbanonymous@gmail.com"; //default email address goes here
    private static final String password = "1q2w3e4r!@"; //default email address password goes here
    private String email = "jayhuanryu@gmail.com"; // Receiving email address here
    private String subject = ""; // Email subject
    private String message = ""; // Email message

    public AnonymousTipsFragment(){}

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState) {
        final View rootView = inflater.inflate(R.layout.fragment_anonymoustips, container, false);
        final EditText et_subject = (EditText) rootView.findViewById(R.id.et_anonymoustips_subject);
        final EditText et_message = (EditText)rootView.findViewById(R.id.et_anonymoustips_message);

    /* Clear Button */
        ((Button) rootView.findViewById(R.id.clearButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              et_subject.setText("");
                et_message.setText("");
            }
        });

        /* Send Button */
        ((Button) rootView.findViewById(R.id.sendButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //declaring & storing subject

                subject = et_subject.getText().toString();

                //declaring & stroing message

                message = et_message.getText().toString();


                Toast.makeText(rootView.getContext(), "Send Button Clicked", Toast.LENGTH_LONG).show();


                //to check if there is any notes
                if(message.length() <= 0) {
                    Toast.makeText(rootView.getContext(), "Missing Notes", Toast.LENGTH_LONG).show();
                } else if (!isWIFIConnected()) {
                    Toast.makeText(rootView.getContext(), "Internet Access Failure", Toast.LENGTH_LONG).show();
                }
                else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    alert.setTitle("Warning");
                    alert.setMessage("Once you click continue, you cannot cancel. Tap \"Continue\" to send");
                    alert.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendEmail(email, subject, message);
                            Toast.makeText(getContext(), "Sent Successfully", Toast.LENGTH_LONG).show();
                        }
                    });
                    alert.setNegativeButton("Cancel", null);
                    alert.create();
                    alert.show();
                }
            }
        });






        return rootView;
    }

    private boolean isWIFIConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private void sendEmail(String email, String subject, String message) {
        Session session = createSessionObject();

        try {
            Message msg = createMessage(email, subject,message,session);
            new SendMailTask().execute(msg);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private Session createSessionObject() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        return Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
    }

    private Message createMessage(String email, String subject, String message, Session session) throws MessagingException,UnsupportedEncodingException {
        Message realmsg = new MimeMessage(session);
        realmsg.setFrom(new InternetAddress("csusbanonymous@gmail.com", "Anonymous"));
        realmsg.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        realmsg.setSubject(subject);
        realmsg.setText(message);
        return realmsg;
    }

    private class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //   progressDialog = ProgressDialog.show(getApplicationContext(),"Please wait","Sending Mail",true,false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Message... params) {
            try {
                Transport.send(params[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

}
